use std::{env, io};
use std::io::{Write, Read};
use std::fs::OpenOptions;

struct Task {
    name: String,
    task: String,
    date: String,
}

impl Task {
    // format to be like 'NAME|TASK|DATE'
    fn format(&self) -> String{
        let r = format!("{}|{}|{}\n", self.name, self.task, self.date);
        r
    }
    pub fn save(&self, f: &String) {
        let mut file = OpenOptions::new().append(true).create(true).open(f.as_str()).expect("create failed");
        file.write_all(self.format().as_bytes()).expect("failed to write");
    }
}
fn read_file(f: &String) -> String {
    let mut file = OpenOptions::new().read(true).open(f.as_str()).expect("can't open file");
    let mut contents = String::new();
    file.read_to_string(&mut contents).expect("can't read file");
    contents
}
fn std_input() -> String {
    let mut input = String::new();
    loop {
    	io::stdin().read_line(&mut input).expect("failed stdin");
    	if input.contains("|") {
        	println!("please, don't use '|'");
        	input.clear();
    	}
    	else if input.is_empty() || input == "\n" {
        	println!("put something!");
        	input.clear();
    	}
    	else {
        	input.pop();
        	break;
    	}
    }
    input
}
fn delete_task(f :&String, n: u16){
    let content = read_file(f);
    let mut file = OpenOptions::new().write(true).open(f.as_str()).expect("failed to open file");
    let mut i:u16 = 0;
    let mut result = String::new();
    for l in content.lines() {
        i += 1;
        if i == n {
            let d: Vec<&str> = l.split("|").collect();
            println!("the task {} has been deleted.", d[0]);
            continue;
        }
        else {
            result.push_str(l);
        }
    }
    file.write_all(result.as_bytes()).expect("failed to write file");
}
fn print_task_list(s: String){
    let mut i:u16 = 0;
    for l in s.lines() {
        i += 1;
        let t: Vec<&str> = l.split("|").collect();
        println!("#{} - {}", i, t[0]);
        println!("{}", t[1]);
        println!("{}\n", t[2]);
    }
}
fn main(){
    let help :&str = "Usage: paja OPTIONS [FILE]\n\nOptions:\n  add\t\t\tadd a new task\n  delete [T_NUM]\tdelete a task\n  list\t\t\tlist all task\n\n  if you don't set FILE, ~/.pajadb will be created\n";
    let def_path = &String::from(".pajadb");
    let args: Vec<String> = env::args().collect();
    if args.len() > 1 {
        if args[1].eq(&String::from("--help"))
        || args[1].eq(&String::from("-h")) {
            println!("{}", help);
        }
        else if args[1].eq(&String::from("add")){
            let mut new_task = Task {
                name: String::new(),
                task: String::new(),
                date: String::new(),
            };
            println!("Name: ");
            new_task.name = std_input();
            println!("Task: ");
            new_task.task = std_input();
            println!("Date [dd/mm]: ");
            new_task.date = std_input();
            if args.len() > 2 {
                new_task.save(&args[2]);
            }
            else {
                new_task.save(def_path);
            }
        }
        else if args[1].eq(&String::from("delete")){
            if args.len() > 2 {
                let num = args[2].trim().parse::<u16>().expect("the argument isn't a number");
                if args.len() > 3 {
                    delete_task(&args[3], num);
                }
                else {
                    delete_task(def_path, num);
                }
            }
            else {
                println!("expected argument after {}", &args[1]);
            }
        }
        else if args[1].eq(&String::from("list")){
            if args.len() > 2 {
                print_task_list(read_file(&args[2]));
            }
            else {
                print_task_list(read_file(def_path));
            }
        }
        else {
            println!("paja: invalid option '{}'\nTry 'paja --help' for more information.", &args[1]);
        }
    }
    else {
        println!("{}", help);
    }
}
